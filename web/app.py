from flask import Flask, render_template, request

app = Flask(__name__)

@app.route("/")
def index():
    return error_404()

@app.route("/<path:name>")
def page(name):
    url = request.environ['REQUEST_URI']
    
    if (url.startswith("/..")) or (url.startswith("/~")) or (url.startswith("//")):
        return error_403(name)
    
    try:
        return render_template(name)
    except:
        return error_404(name)
   

@app.errorhandler(403)
def error_403(url):
    return render_template("403.html")

@app.errorhandler(404)
def error_404(url=""):
    if ("//" in url):
        return error_403(url)
    return render_template("404.html")

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
